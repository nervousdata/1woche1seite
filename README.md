# 1 Woche 1 Seite
## Wochenkalender A5 zum Selbstdrucken. / Weekly calendar A5 for self-printing. / Calendario semanal A5 para autoimpresión.
**Files for 2023**

Typeset in LaTeX (using TikZ). After making the .pdf file, run [pdfjam](https://github.com/rrthomas/pdfjam) to arrange the pages as a booklet for printing on A4 paper.

```
pdfjam a5_1woche1seite_de.pdf --booklet true --paper a4 --landscape --outfile a5_1woche1seite_de_druck.pdf
```
When changing languages in the .tex file, the titles for the months need to be edited manually. Otherwise this block in the preamble should work:
```
\documentclass[spanish,11pt]{letter}
\usepackage[spanish]{babel}
\usepackage[spanish]{translator}
```

Printer settings: duplex, long edge, landscape format, scale 100% (actual size)

Font: [Latin Modern Mono](https://tug.org/FontCatalogue/latinmodernmono/)

### Deutsch
1 Woche auf 1 Seite. Mit schräg laufenden Trennlinien. Zum Ausdrucken auf A4-Druckern, am besten mit Duplexfunktion. Mit Feiertagen (hauptsächlich christliche) und einheitlichen Terminen wie Winter- und Sommerzeit.

* [PDF](a5_1woche1seite_de_druck.pdf) für Broschürendruck auf 14 Seiten A4 Papier (ergibt 56 Seiten, inkl. drei leere Seiten am Ende). Seiten in der Mitte falten und alle ineinander legen.
* [PDF](a5_1woche1seite_de.pdf) (53 Seiten) mit Einzelseiten

### English
1 week on 1 page. With slanting lines. Printable on A4 printers (best with duplex function). Empty, without holidays etc.

* [PDF](a5_1woche1seite_en_print.pdf) for booklet printing on 14 sheets A4 paper (makes 56 pages, incl. three empty pages at the end). Fold sheets in the middle and put them all into each other.
* [PDF](a5_1woche1seite_en.pdf) (53 pages) with single pages

### Español
1 semana en 1 página. Con líneas inclinadas. Para imprimir en impresoras A4 (mejor con función duplex). Vacío, sin días festivos etc.

* [PDF](a5_1woche1seite_es_impr.pdf) para imprimir folleto en 14 hojas de papel DIN A4 (hace 56 páginas, incluidas tres páginas vacías al final).
* [PDF](a5_1woche1seite_es.pdf) (53 páginas) con páginas individuales

### Example of one page for booklet printing (2022)
![Example of the layout on two pages.](a5_1woche1seite_bsp.png "Example of one page from the booklet.")
